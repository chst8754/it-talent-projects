# IT-Talents Projects

## [https://www.it-talents.de/foerderung/code-competition/](https://www.it-talents.de/foerderung/code-competition/)

This repository acts as an overview of all the attempted projects [I](https://www.linkedin.com/in/christopherstarck/) will take part in. 


### Project 1 *Oktober 2017* : Mühle / Nine Men's Morris

#### Requirements
* Playing field with user interaction
* Some sort of visual feedback (GUI or console)
* AI opponent
* Implement the rules of Mühle/Nine Men's Morris

#### Evaluation Criteria
* Functionality : Does the program run? Are there any bugs?
* Code quality and coding style  : Is the code well structured and efficient?
* Documentation : Is the code well documented? 
* Additional Features : Nice to have features will also be considered. 

#### Rough Documentation
* [Wikipedia Rules](https://en.wikipedia.org/wiki/Nine_Men%27s_Morris)